README for lemannl


## Description

Simplified implementation a neural network library.
For use in presentations explaining the implementation of neural networks.
The library is heavily inspired by [autograd][] and [pytorch][],
although it does share very few implementation details.

[autograd]: https://github.com/HIPS/autograd
[pytorch]: https://pytorch.org/

The library uses `numpy` alone, and nothing else, to implement feed
forward neural networks that can be hyper-parametrized in several fashions.
It supports deep layering, and common activation functions.
Also allows for the possibility of adding one's own activation functions.


## Copying

Copyright (C) 2018 Michal Grochmal

This file is part of `lemannl`, which is distributed under the MIT license.
A copy of the license is included in the LICENSE file.

