#!/usr/bin/env python

import inspect
from functools import wraps
import operator as op
import numpy as np


def drop_wrapper(*args):
    return [a.val if isinstance(a, Node) else a for a in args]


# need to write tests for these
node_functions = [
    np.mean,
    np.log,
    np.tanh,
    # the ones below do not yet have VJPs
    np.empty,
    np.ones,
    np.zeros,
    np.full,
    np.empty_like,
    np.ones_like,
    np.zeros_like,
    np.full_like,
]

class NumpyWrap(object):
    _npg = None

    def __new__(cls):
        """Use new to return a singleton, note this cannot have an init."""
        if not cls._npg:
            cls._npg = super(NumpyWrap, cls).__new__(cls)
        return cls._npg

    def __getattr__(self, key):
        """
        Converts the arguments of numpy functions to numpy arrays.

        Gives the correct AttributeError on failure.
        """
        fn = getattr(np, key)
        @wraps(fn)
        def np_fn_wrapper(*args, **kwargs):
            fn_args = drop_wrapper(*args)
            fn_values = drop_wrapper(*kwargs.values())
            kwargs = dict(zip(kwargs.keys(), fn_values))
            ret = fn(*fn_args, **kwargs)
            if fn in node_functions:
                return Node(ret, fn, list(args))
            return ret
        return np_fn_wrapper

    def array(self, *args, **kwargs):
        """
        Use a graph node for every single array.

        This avoids numpy bug where:
        np.array([[1, 1]]) @ Node([[1], [1]]) does
        not invoke Node.__rmatmul__
        https://github.com/numpy/numpy/issues/9028

        """
        return Node(*args, **kwargs)


npg = NumpyWrap()


class Node(object):
    _max_id = 0

    def __new__(cls, val, func=None, parents=None, *args, **kwargs):
        if isinstance(val, Node):
            return val
        obj = super(Node, cls).__new__(cls)
        kwargs['dtype'] = np.float64
        obj.val = np.array(val, **kwargs)
        obj.func = func
        obj.func_name = None
        if func:
            obj.func_name = func.__name__
        if not parents:
            parents = []
        obj.parents = [Node(x) for x in parents]
        obj.grads = None
        Node._max_id += 1
        obj.id = Node._max_id
        return obj

    def add_grad(self, grad):
        if not self.grads:
            # MEMORY LEAK!  But how can I allow for 2nd derivs?
            self.grads = Node(npg.zeros_like(self))
            #self.grads = npg.zeros_like(self)
        self.grads = self.grads + grad

    def __eq__(self, other):
        if not isinstance(other, Node) and not isinstance(other, np.ndarray):
            return False
        return npg.allclose(self, other)

    def __str__(self):
        return str(self.val)

    def __repr__(self):
        return repr(self.val)

    def graph_str(self):
        form = ['N {}'.format(self.id),
                'Value:',
                str(self.val),
                'Fn: {}'.format(self.func_name),
                'Grads:']
        if self.grads:
            form.append(str(self.grads))
        form.append( 'Parents:')
        for p in self.parents:
            form.append(str(p.val))
        return '\n'.join(form)

    def graphviz(self, graph):
        for p in self.parents:
            graph.append(f'    {p.id} -> {self.id}')
            p.graphviz(graph)
        label = self.graph_str().replace('\n', '\\n')
        graph.append(f'    {self.id} [label="{label}"]')

    def digraph(self):
        graph = ['digraph nodes {']
        self.graphviz(graph)
        graph.append('}')
        return '\n'.join(graph)

    def __add__(self, other):
        o = Node(other)
        return Node(self.val + o.val, op.add, [self, o])

    def __radd__(self, other):
        o = Node(other)
        return Node(o.val + self.val, op.add, [o, self])

    def __sub__(self, other):
        o = Node(other)
        return Node(self.val - o.val, op.sub, [self, o])

    def __rsub__(self, other):
        o = Node(other)
        return Node(o.val - self.val, op.sub, [o, self])

    def __neg__(self):
        return Node(-self.val, op.neg, [self])

    def __mul__(self, other):
        o = Node(other)
        return Node(self.val * o.val, op.mul, [self, o])

    def __rmul__(self, other):
        o = Node(other)
        return Node(o.val * self.val, op.mul, [o, self])

    def __truediv__(self, other):
        o = Node(other)
        return Node(self.val / o.val, op.truediv, [self, o])

    def __rtruediv__(self, other):
        o = Node(other)
        return Node(o.val / self.val, op.truediv, [o, self])

    def __pow__(self, other):
        o = Node(other)
        return Node(self.val ** o.val, op.pow, [self, o])

    def __rpow__(self, other):
        o = Node(other)
        return Node(o.val ** self.val, op.pow, [o, self])

    def __matmul__(self, other):
        o = Node(other)
        return Node(self.val @ o.val, op.matmul, [self, o])

    def __rmatmul__(self, other):
        o = Node(other)
        return Node(o.val @ self.val, op.matmul, [o, self])

    @property
    def T(self):
        return Node(self.val.T, np.transpose, [self])

    @property
    def shape(self):
        return self.val.shape


def walk_nodes(n):
    graph = [(n.id, n)]
    for p in n.parents:
        graph += walk_nodes(p)
    return list(dict(graph).items())


def do_grad(f, argnums=0, keepgraph=False):
    if 'all' == argnums:
        spec = inspect.getfullargspec(f)
        argnums = list(range(len(spec.args)))
    if isinstance(argnums, int):
        argnums = [argnums]
    @wraps(f)
    def grad_func(*args, **kwargs):
        wrt = {}
        args = list(args)
        for i in argnums:
            wrt[i] = args[i] = Node(args[i])
        ret = f(*args, **kwargs)
        # initial gradient (currently a hack, should implement ones_like)
        ret.add_grad(Node(npg.ones_like(ret.val, dtype=np.float64)))
        nodes = sorted(walk_nodes(ret), key=lambda x: x[0], reverse=True)
        for n_id, n in nodes:
            if n.func:
                vjp = vjps[n.func]
                vjp(n, n.parents)
        grads = [wrt[x].grads for x in argnums]
        if len(grads) == 1:
            grads = grads[0]
        if keepgraph:
            return grads, ret
        else:
            # zero the grads, in case node-arrays exist in the loss
            for _, n in nodes:
                n.grads = []
        return grads
    return grad_func


vjps = {}


def vjp_add(node, parents):
    grad = npg.sum(node.grads)
    for p in parents:
        p.add_grad(node.grads)
vjps[op.add] = vjp_add


def vjp_sub(node, parents):
    grad = npg.sum(node.grads)
    l, r = parents
    l.add_grad(node.grads)
    r.add_grad(-node.grads)
vjps[op.sub] = vjp_sub


def vjp_neg(node, parents):
    grad = npg.sum(node.grads)
    parents[0].add_grad(-node.grads)
vjps[op.neg] = vjp_neg


def vjp_mul(node, parents):
    grad = npg.sum(node.grads)
    l, r = parents
    l.add_grad(node.grads * node.val / l.val)
    r.add_grad(node.grads * node.val / r.val)
vjps[op.mul] = vjp_mul


def vjp_truediv(node, parents):
    grad = npg.sum(node.grads)
    l, r = parents
    l.add_grad(node.grads / r.val)
    r.add_grad(-1 / node.grads**2)
vjps[op.truediv] = vjp_truediv


def vjp_pow(node, parents):
    grad = npg.sum(node.grads)
    l, r = parents
    l.add_grad(node.grads * r.val * l.val**(r.val - 1))
    # log(-x) would give us a complex number,
    # instead we support even powers or positive bases only
    r.add_grad(node.grads * np.log(npg.abs(l.val)) * l.val**r.val)
vjps[op.pow] = vjp_pow

#defvjp(anp.power,
#    lambda ans, x, y : unbroadcast_f(x, lambda g: g * y * x ** anp.where(y, y - 1, 1.)),
#    lambda ans, x, y : unbroadcast_f(y, lambda g: g * anp.log(replace_zero(x, 1.)) * x ** y))


def vjp_matmul(node, parents):
    grad = npg.sum(node.grads)
    l, r = parents
    #l.add_grad(grad * r.val.T)
    l.add_grad(node.grads @ r.T)
    #r.grads.append((grad.T @ l.val).T)
    r.add_grad(l.T @ node.grads)
vjps[op.matmul] = vjp_matmul


def vjp_mean(node, parents):
    grad = npg.sum(node.grads)
    p = parents[0]
    p.add_grad(node.grads / npg.full(p.val.shape, p.val.size / node.val.size))
vjps[np.mean] = vjp_mean


def vjp_tanh(node, parents):
    grad = npg.sum(node.grads)
    p = parents[0]
    p.add_grad(node.grads * 1 - npg.tanh(npg.tanh(p)))
vjps[np.tanh] = vjp_tanh


if '__main__' == __name__:
    n = Node(7., op.add, [3., 4])
    print(n.digraph())

