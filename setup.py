#!/usr/bin/env python3

from setuptools import setup, find_packages
import os

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

# Full list of classifiers can be found here:
# https://pypi.org/pypi?%3Aaction=list_classifiers
CLS = [
  'License :: OSI Approved :: MIT License',
  'Intended Audience :: Education',
  'Development Status :: 3 - Alpha',
  'Environment :: Console',
  'Operating System :: OS Independent',
  'Programming Language :: Python',
  'Topic :: Scientific/Engineering :: Mathematics',
]
REQS = (
    'numpy >= 1.12.0',
)
TEST_REQS = (
    'pytest >= 4.0.0',
    'tox >= 3.0.0',
)
SRC = 'src'
TESTS = ('*.tests', 'tests.*', '*.tests.*', 'tests')
PKG_DATA = {
    'lemannl': ['*.md'],
}

setup(
    name='lemannl',
    description='simplified ann library, for presentaion purposes',
    version='0.1',
    author='Michal Grochmal',
    author_email='NmiOkeS@PgroAchmalM.org',
    license='MIT',
    url='https://gitlab.com/grochmal/lemannl',
    long_description=read('README'),
    packages=find_packages(SRC, exclude=TESTS),
    package_dir={'': SRC},
    include_package_data=True,
    package_data=PKG_DATA,
    classifiers=CLS,
    install_requires=REQS,
    tests_require=TEST_REQS,
    extras_require={'test': TEST_REQS},
)

