#!/usr/bin/env python

import numpy as np
import operator as op
from lemannl import grad
from lemannl.grad import npg


def test_build_node():
    n = grad.Node(7)
    assert n
    assert n == npg.array(7.)
    assert n.val == np.array(7.)
    assert n.id == 1


def test_new():
    n = grad.Node(grad.Node(grad.Node(7)))
    assert n


def test_parents():
    n = grad.Node(7., op.add, [3, 4])
    assert isinstance(n.parents[0], grad.Node)
    assert n.parents[0] == npg.array(3.)


def test_repr():
    n = grad.Node(7., op.add, [3, 4])
    assert str(n) == str(n.val)
    assert repr(n) == repr(n.val)


def test_equal():
    n1 = grad.Node(7., op.add, [3, 4])
    n2 = grad.Node(7., op.add, [3, 4])
    n3 = grad.Node(7.)
    assert n1 == n2
    assert not n1.id == n3.id
    assert n1 == npg.array(7.)
    assert not n1 == 7.


def test_graphviz():
    expected = """
digraph nodes {
    1 -> 3
    1 [label="N 1\\nValue:\\n3.0\\nFn: None\\nGrads:\\nParents:"]
    2 -> 3
    2 [label="N 2\\nValue:\\n4.0\\nFn: None\\nGrads:\\nParents:"]
    3 [label="N 3\\nValue:\\n7.0\\nFn: add\\nGrads:\\nParents:\\n3.0\\n4.0"]
}
"""
    grad.Node._max_id = 0
    n = grad.Node(7, op.add, [3, 4])
    digraph = '\n' + n.digraph() + '\n'
    assert expected == digraph


def test_walk():
    n = grad.Node(3)
    assert grad.walk_nodes(n) == [(n.id, n)]
    x = grad.Node(3, op.add, [1, 2])
    assert len(grad.walk_nodes(x)) == 3
    x = grad.Node(3, op.add, [grad.Node(1, op.sub, [2, 1]), 2])
    assert len(grad.walk_nodes(x)) == 5


def test_operations():
    n = grad.Node(7)
    g = 3 + n + 1
    assert len(grad.walk_nodes(g)) == 5
    g = n - 7
    assert len(grad.walk_nodes(g)) == 3
    g = -n
    assert len(grad.walk_nodes(g)) == 2
    g = 3 * n + 2
    assert len(grad.walk_nodes(g)) == 5
    g = n/3 + 1
    assert len(grad.walk_nodes(g)) == 5
    g = n**2
    assert len(grad.walk_nodes(g)) == 3


def test_matmul():
    n = grad.Node([[1, 7]])
    g = n @ n.T
    assert len(grad.walk_nodes(g)) == 3
    g = n @ npg.array([[1], [7]])
    assert len(grad.walk_nodes(g)) == 3


def test_grad_add():
    f = lambda x: x + 3
    fg = grad.do_grad(f)
    assert fg(3) == npg.array(1.)
    f = lambda x: x + x
    fg = grad.do_grad(f)
    assert fg(3) == npg.array(2.)


def test_grad_vecmul():
    f = lambda x, y: x @ y
    fg = grad.do_grad(f, 0)
    m = npg.array([[2, 3, 4],
                   [7, 8, 9]])
    v = npg.array([[10],
                   [20],
                   [30]])
    expected = npg.array([[10., 20., 30.],
                          [10., 20., 30.]])
    assert npg.allclose(fg(m, v), expected)
    fg = grad.do_grad(f, 1)
    expected = npg.array([[9.],
                          [11.],
                          [13.]])
    assert npg.allclose(fg(m, v), expected)


def test_first_derivs():
    n = grad.Node([1, 7])
    f = lambda x: 3 * x
    fg = grad.do_grad(f, 0)
    grads = fg(n)
    assert npg.allclose(grads, [3, 3])
    nodes = sorted(grad.walk_nodes(grads))
    print(nodes)
    print(nodes[-1][1].digraph())
    assert nodes == [(1, n), (2, grad.Node(3))]


def test_matmul():
    f = lambda x, y: x @ y
    fg = grad.do_grad(f, 0)
    m = npg.array([[2, 3, 4, 2],
                   [7, 8, 9, 7]])
    v = npg.array([[10, 70, 10],
                   [20, 80, 20],
                   [50, 50, 50],
                   [30, 90, 30]])
    expected = npg.array([[90., 120., 150., 150.],
                          [90., 120., 150., 150.]])
    assert npg.allclose(fg(m, v), expected)
    fg = grad.do_grad(f, 1)
    expected = npg.array([[ 9.,  9.,  9.],
                          [11., 11., 11.],
                          [13., 13., 13.],
                          [ 9.,  9.,  9.]])
    assert npg.allclose(fg(m, v), expected)


def test_grad_ann():
    i = npg.array([[0.3],
                   [0.1],
                   [0.5]])
    l1 = npg.array([[0.3,  0.1,  0.2],
                    [0.2, -0.1, -0.1],
                    [0.7,  0.5, -0.3],
                    [0.5,  0.5, -0.5]])
    l1b = npg.array([[0.3],
                     [0.2],
                     [0.2],
                     [0.3]])
    l2 = npg.array([[0.2,  0.3,  0.1, 0.1],
                    [0.7, -0.2, -0.1, 0.3]])
    l2b = npg.array([[ 0.3],
                     [-0.2]])
    y = npg.array([[1.],
                   [0.]])
    act = lambda x: 2*x
    def loss(y, i, l1, l1b, l2, l2b):
        #return y - 2*(l2 @ 2*(l1 @ i + l1b) + l2b)
        y_hat = act(l2 @ act(l1 @ i + l1b) + l2b)
        #print('YHAT', y_hat)
        #return (y - y_hat)**2
        return npg.mean((y - y_hat)**2)
        #return y - y_hat
    #print('GRAD', y - act(l2 @ act(l1 @ i + l1b) + l2b))
    #ret = loss(y, i, grad.Node(l1), l1b, grad.Node(l2), l2b)
    #print('RET', ret)
    # numpy bug 9028 :)
    #fg = grad.do_grad(loss, [0, 1, 2, 3, 4, 5])
    fg = grad.do_grad(loss, argnums='all', keepgraph=True)
    grads, graph = fg(y, i, l1, l1b, l2, l2b)
    y_grad = npg.array([[-0.464],
                        [-1.016]])
    i_grad = npg.array([[1.46144],
                        [ 0.9392],
                        [0.03264]])
    l1_grad = npg.array([[  0.9648,   0.3216,   1.608],
                         [ -0.0768,  -0.0256,  -0.128],
                         [-0.06624, -0.02208, -0.1104],
                         [ 0.42144,  0.14048,  0.7024]])
    l1b_grad = npg.array([[  3.216],
                          [ -0.256],
                          [-0.2208],
                          [ 1.4048]])
    l2_grad = npg.array([[0.928, 0.3712, 0.57536, 0.464],
                         [2.032, 0.8128, 1.25984, 1.016]])
    l2b_grad = npg.array([[0.928],
                          [2.032]])
    assert grads[0] == y_grad
    assert grads[1] == i_grad
    assert grads[2] == l1_grad
    assert grads[3] == l1b_grad
    assert grads[4] == l2_grad
    assert grads[5] == l2b_grad
    #print('ALL GRADS')
    #for g in grads:
    #    print(g)
    #print(graph.digraph())
    #assert False


def test_cleanup():
    pass

